

/* DÉCLARATION DES TIMESLINES ANIME.JS */
var timelineScene01 = anime.timeline({
    duration: 700,
    easing: "linear",
    delay: 100
})

/* ANIMATIONS ANIME.JS avec CallBacks*/
// Soleil s'élève
anime({
    targets: "#soleil",
    translateX: 50,
    translateY: -320,
    scale: 3.2,
    easing: 'easeInOutBack'
});
// Vieil arbre marche vers l'ambulance
timelineScene01.add({
    targets: "#vieil-arbre",
    translateX: 100,
    delay: 200
})
    .add({
        targets: "#vieil-arbre",
        translateX: 200,
        delay: 200
    })
    .add({
        targets: "#vieil-arbre",
        translateX: 300,
        delay: 200
    })
    // Vieil arbre saute dans l'ambulance
    .add({
        targets: "#vieil-arbre",
        translateX: 650,
        translateY: -110,
        scale: 0,
        rotate: '0.3turn',
        delay: 200
    })
    // L'ambulance démarre
    .add({
        targets: "#ambulance",
        translateX: 850,
    })
    // SCÈNE 1 fondu noir
    .add({
        targets: "#scene1",
        opacity: 0,
        delay: 200
    })
    .add({
        targets: "#message1",
        opacity: 1,
        delay: 200
    })

    //Écoboite est amie avec la nature depuis longtemps (aparaît)
    .add({
        targets: "#message_1_1",
        scale: 1.5,
        opacity: 1,
        duration: 1000,
        delay: 200
    })
    //Nous traitons nos arbres non pas dans une usine, mais dans un atelier! (aparaît)
    .add({
        targets: "#message_1_2",
        scale: 1.5,
        opacity: 1,
        duration: 1000,
        delay: 200
    }).add({
        //Écoboite est amie avec la nature depuis longtemps (disaparaît)
        targets: "#message_1_1",
        scale: 0.1,
        opacity: 0,
        duration: 1000,
        delay: 200
    })
    //Nous traitons nos arbres non pas dans une usine, mais dans un atelier! (disaparaît)
    .add({
        targets: "#message_1_2",
        scale: 0.1,
        opacity: 0,
        duration: 1000,
        delay: 200
    })
    //page message disparaît
    .add({
        targets: "#message1",
        opacity: 0,
        duration: 200,
        delay: 200
    })
    //  FIN MESSAGE 1
    //  DEBUT SCENE 2
    
    .add({
        targets: "#scene2",
        opacity: 1,
        duration: 1000,
        delay: 200
    })
    .add({
        targets: "#arbre",
        opacity: 10,
        translateX: 150,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#sort1",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#sort2",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#sort3",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#sort4",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#cloud",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#arbre",
        opacity: 0,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#arbre_coupe",
        opacity: 1,
        duration: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#cloud",
        opacity: 0,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#t1",
        translateX: -30,
        duration: 10,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#t4",
        translateX: 30,
        translateY: -10,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#t3",
        translateX: 30,
        translateY: 20,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#cloud",
        opacity: 1,
        scale: 1,
        easing: "linear",
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#arbre_coupe",
        opacity: 0,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#boites",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#cloud",
        opacity: 0,
        scale: 1,
        easing: "linear",
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#boite4",
        translateX: 150,
        duration: 200,
        delay: 200
    }).add({
        targets: "#boite3",
        translateX: 150,
        duration: 200,
        delay: 200
    }).add({
        targets: "#boite2",
        translateX: 150,
        duration: 200,
        delay: 200
    }).add({
        targets: "#boite1",
        translateX: 150,
        duration: 200,
        delay: 200
    }).add({
        targets: "#scene2",
        opacity: 0,
        delay: 200
    })
    //  FIN SCENE 2
    //  DEBUT MESSAGE 2
    .add({
        targets: "#message2",
        opacity: 1,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#message_2_1",
        scale: 1.5,
        opacity: 1,
        duration: 1000,
        delay: 200
    })
    .add({
        targets: "#message_2_2",
        scale: 1.5,
        opacity: 1,
        duration: 1000,
        delay: 200
    }).add({
        targets: "#message_2_1",
        scale: 0.1,
        opacity: 0,
        duration: 1000,
        delay: 200
    })
    .add({
        targets: "#message_2_2",
        scale: 0.1,
        opacity: 0,
        duration: 1000,
        delay: 200
    })
    .add({
        targets: "#message2",
        opacity: 0,
        duration: 200,
        delay: 200
    })
    .add({
        targets: "#scene3",
        opacity: 1,
        delay: 200
    })
    
//Changement de climat
const container = document.querySelector(".climat");
for (let i = 0; i <= 200; i++) {
    const pluie = document.createElement("div");
    pluie.classList.add("pluie");
    container.appendChild(pluie)
}
function ilPleut() {
    anime({
        targets: ".pluie",
        translateX: function () {
            return anime.random(-500, 8500)
        },
        translateY: function () {
            return anime.random(1200, 1500)
        },
        easing: "linear",
        duration: 5000,
        delay: anime.stagger(30),
        complete: ilPleut
    })
}
ilPleut();